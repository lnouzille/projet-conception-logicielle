# Projet de conception de logiciel : Site web de météo

## Description 

Ce site météo a pour objectif de renvoyer à un utilisateur qui entre son nom et sa localisation, 
la météo où il se trouve et plus précisement la température qu'il fait et une image traduisant 
le temps à sa localisation. Si la localisation entrée est introuvable, le site renvoie que la localisation est incorrecte et aucune information météo ne s'affiche.

## Organisation de l'archive 

L'utilisateur remplit le formulaire sur le site. Ses informations sont récupérées par le site avec une requête POST puis envoyées à l'API pour récupérer les données météorologiques via une requête HTTP. Les informations sont ensuite affichées sur le site de façon claire et simple.

```mermaid
graph TD
    A(Utilisateur) -->|POST| B(Site Web)
    B(Site Web) -->|HTTP| C(API)
```

L'application possède 4 principaux packages : 
- application : contient l'accès aux données (dao), tout ce qui est spécifique à l'applicatif (model), la couche de présentation (view) et les tests unitaires (tests).
- Meteo : contient les packets python effectif du projet. C'est le nom du paquet Python que vous devrez utiliser pour importer ce qu'il contient.
- static : contient les icônes météo et le design du site.
- templates\application : contient tout le code WEB.

## Lancement de l'application 

Etape 1 : Installation des packages - `pip install -r requirements.txt`

Etape 2 : Appliquer les migrations - `python manage.py migrate`

Etape 3 : Lancer l'application - `python manage.py runserver`

Etape 4 : Pour accéder au site web, suivre le lien affiché dans la sortie de l'étape 3. Exemple :  
```C:...\projet-conception-logicielle> python manage.py runserver
Watching for file changes with StatReloader  
Performing system checks...  

System check identified no issues (0 silenced).  
March 24, 2023 - 14:45:53  
Django version 4.1.5, using settings 'Meteo.settings'  
Starting development server at http://127.0.0.1:8000/ *LIEN ICI* 
Quit the server with CTRL-BREAK.
```

## Lancement des tests

`python -m unittest discover application/tests -p  "*_test.py"`

## Icônes

Toutes nos icônes/images viennent de https://icons.getbootstrap.com/. 

