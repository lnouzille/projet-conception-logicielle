import unittest

from application.dao.apimeteo import APIMeteo


class APIMeteoTest(unittest.TestCase):
    
    def test_get_meteo(self):
        attendu = 4
        actuel = APIMeteo("Bruz").get_meteo()
        self.assertEqual(attendu, len(actuel))
        self.assertIsInstance(actuel['temperature'], float)
        self.assertIsInstance(actuel['code'], int)
        self.assertIsInstance(actuel['heure'], str)
        self.assertIsInstance(actuel['heure_journee'], list)
    
    def test_get_meteo_fail(self): 
        meteo = APIMeteo("Bjfenfjner").get_meteo()
        self.assertIsNone(meteo)

if __name__ == "__main__":
    unittest.main()
