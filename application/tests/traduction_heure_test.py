import unittest

from application.dao.apimeteo import APIMeteo
from application.model.traduction_heure import HeureTraduction


class HeureTraductionTest(unittest.TestCase):
    
    def test_is_day(self):
        resultat_API = APIMeteo("Bruz").get_meteo()
        resultat_traduction = HeureTraduction.is_day(resultat_API)
        self.assertIsInstance(resultat_traduction, bool)

if __name__ == "__main__":
    unittest.main()