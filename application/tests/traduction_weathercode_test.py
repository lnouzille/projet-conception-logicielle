import unittest

from application.dao.apimeteo import APIMeteo
from application.model.traduction_weathercode import WeatherCodeTraduction
from application.model.traduction_heure import HeureTraduction


class WeatherCodeTraductionTest(unittest.TestCase):
    
    def test_weather_code(self):
        resultat_API = APIMeteo("Bruz").get_meteo()
        resultat_traduction = WeatherCodeTraduction.weathercode_image(resultat_API)
        self.assertIsInstance(resultat_traduction, str)

if __name__ == "__main__":
    unittest.main()