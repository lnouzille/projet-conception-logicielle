from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from application.dao.apimeteo import APIMeteo
from application.model.traduction_weathercode import WeatherCodeTraduction
from application.model.traduction_heure import HeureTraduction


@csrf_exempt
def home(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        prenom = request.POST.get("prenom")
        localisation = request.POST.get("localisation")
        resultat_API = APIMeteo(localisation).get_meteo()
        if resultat_API is None : 
            localisation = "incorrecte"
            temperature = "incorrecte"
            image = "bug.svg"
        else : 
            temperature = resultat_API["temperature"]
            image = WeatherCodeTraduction.weathercode_image(resultat_API)
        return render(request, "application/result.html", context={"prenom": prenom, 
                                                                   "localisation" : localisation,
                                                                   "temperature" : temperature,
                                                                   "temps" : image})
    return render(request, "application/home.html")

@csrf_exempt
def result(request: HttpRequest) -> HttpResponse:
    return render(request, "application/result.html")