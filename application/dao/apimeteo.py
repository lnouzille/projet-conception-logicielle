import requests as req

class APIMeteo:
    """ Obtenir la météo d'un lieu """

    def __init__(self, localisation : str) -> None:
        """ Constructeur 

        Args:
            localisation (str): localisation de la météo à récupérer 
        """        
        self.path_meteo = "https://api.open-meteo.com/v1/forecast?latitude={}&longitude={}&timezone={}&current_weather=true&daily=sunrise,sunset"
        self.path_localisation = "https://geocoding-api.open-meteo.com/v1/search?name={}&count=1"
        self.localisation = localisation

    def get_meteo(self) -> dict :
        """ Récupérer la température, le code météo, l'heure de la demande et 
            les heures du lever et du coucher du soleil

        Returns:
            dict: dictionnaire des informations météorologiques
        """        
        # Requête pour obtenir la latitude, la longitude et l'heure d'une localisation
        loc = req.get(self.path_localisation.format(self.localisation))
        res = loc.json()
        # Si la localisation entrée est introuvable : return None 
        if (loc.status_code != 200) or ('results' not in res.keys()) :
            return None
        # Sinon on lance une 2e requête pour obtenir la météo
        loc = res['results'][0]
        lat = loc['latitude']
        long = loc['longitude']
        timezone = loc['timezone']
        current_meteo = req.get(self.path_meteo.format(lat, long, timezone)).json()['current_weather']
        daily = req.get(self.path_meteo.format(lat, long, timezone)).json()['daily']
        temperature = current_meteo['temperature']
        weather_code = current_meteo['weathercode']
        daytime = [daily['sunrise'][0], daily['sunset'][0]]
        time = current_meteo['time']
        return {'temperature' : temperature, 
                'code' : weather_code, 
                'heure' : time, 
                'heure_journee' : daytime}
