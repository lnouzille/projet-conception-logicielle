from datetime import datetime

class HeureTraduction:
    """ Permet de savoir si l'heure de la demande est une heure de jour ou de nuit """

    def is_day(resultat_API : list) -> bool:
        """ Savoir si l'heure de la requête est pendant le jour ou la nuit 

        Args:
            resultat_API (list): résultat de la requête de l'API Météo 

        Returns:
            bool: True si l'heure de la demande est entre le lever et le coucher du soleil
                  et False sinon 
        """        
        heure_actuelle = datetime.fromisoformat(resultat_API['heure'])
        heures_journee = resultat_API['heure_journee']
        debut_journee = datetime.fromisoformat(heures_journee[0])
        fin_journee = datetime.fromisoformat(heures_journee[1])
        if heure_actuelle >= debut_journee and heure_actuelle <= fin_journee : 
            return True 
        return False