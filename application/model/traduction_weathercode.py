from application.model.traduction_heure import HeureTraduction


class WeatherCodeTraduction:
    """ Traduire le code météo en une description par image 
    
    La signification des nombres est décrite en intégralité dans la documentation 
    de l'API Météo 
    """

    def weathercode_image(resultat_API : dict) -> str:
        """ Renvoie le nom de l'image associé au code météo 

        Args:
            resultat_API (dict): résultat de la requête de l'API Météo 

        Returns:
            str: nom de l'image
        """        
        code = resultat_API['code']
        if code == 0 :
            if HeureTraduction.is_day(resultat_API) :
                return 'clear_sky_day.svg'
            else :
                return 'clear_sky_night.svg'
        elif code in [1, 2] :
            if HeureTraduction.is_day(resultat_API) :
                return 'partly_cloudy_day.svg'
            else :
                return 'partly_cloudy_night.svg'
        elif code == 3 :
            return 'overcast.svg'
        elif code in [45, 48] :
            return 'fog.svg'
        elif code in [51, 53, 55, 56, 57, 61, 63, 65, 80, 81, 82] :
            return 'rain.svg'
        elif code in [71, 73, 75, 77, 85, 86, 96, 99] :
            return 'snow.svg'
        else :
            return 'thunder.svg'